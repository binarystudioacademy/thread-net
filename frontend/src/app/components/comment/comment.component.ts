import { Component, Input } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { CommentService } from 'src/app/services/comment.service';
import { User } from '../../models/user';
import { AuthenticationService } from '../../services/auth.service';
import { switchMap, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public comment: Comment;

    public currentUser: User;
    private unsubscribe$ = new Subject<void>();

    public constructor(
        private commentService: CommentService,
        private authService: AuthenticationService,
    ) { }

    public ngOnInit() {
        this.getUser();
    }

    public deleteComment() {
        this.commentService.deleteComment(this.comment.id)
            .subscribe()
    }

    public mineComments(): Boolean {
        return this.comment.author.id == this.currentUser.id
    }

    private getUser() {
        this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => (this.currentUser = user));
    }
}
