﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly CommentService _commentService;

        public CommentsController(CommentService commentService)
        {
            _commentService = commentService;
        }

        [HttpPost]
        public async Task<ActionResult<CommentDTO>> CreatePost([FromBody] NewCommentDTO comment)
        {
            comment.AuthorId = this.GetUserIdFromToken();
            return Ok(await _commentService.CreateComment(comment));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateComment([FromBody] UpdateCommentDTO commentDTO)
        {
            var currentUserId = this.GetUserIdFromToken();
            await _commentService.UpdateComment(commentDTO, currentUserId);
            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteComment(int id)
        {
            var currentUserId = this.GetUserIdFromToken();
            await _commentService.DeleteComment(id, currentUserId);
            return NoContent();
        }
    }
}