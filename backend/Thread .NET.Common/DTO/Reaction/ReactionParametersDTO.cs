﻿namespace Thread_.NET.Common.DTO.Reaction
{
    public class ReactionParametersDTO
    {
        public int PostId { get; set; }
        public bool IsLike { get; set; }
    }
}
