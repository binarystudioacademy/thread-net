﻿using System;

namespace Thread_.NET.BLL.Exceptions
{
    public sealed class NoAccessException : Exception
    {
        public NoAccessException() : base("Access denied") { }

    }
}