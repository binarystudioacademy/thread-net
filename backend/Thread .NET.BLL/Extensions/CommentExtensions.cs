﻿using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Extensions
{
    public static class CommentExtensions
    {
        public static bool IsAuthorComment(this Comment comment, int userId)
        {
            return comment.AuthorId.Equals(userId);
        }
    }
}