﻿using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Extensions
{
    public static class PostExtensions
    {
        public static bool IsAuthorComment(this Post post, int userId)
        {
            return post.AuthorId.Equals(userId);
        }
    }
}
