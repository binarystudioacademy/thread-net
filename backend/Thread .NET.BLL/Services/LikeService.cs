﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.Reaction;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.DAL.Context;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        public LikeService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task LikePost(NewReactionDTO reaction)
        {
            var likes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId);

            if (likes.Any())
            {
                _context.PostReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                return;
            }

            _context.PostReactions.Add(new DAL.Entities.PostReaction
            {
                PostId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
        }

        public async Task<List<UserDTO>> GetPostLikes(ReactionParametersDTO dto)
        {           
            var likes = await _context.PostReactions.Include("User").Where(x => x.PostId == dto.PostId && x.IsLike == dto.IsLike).ToListAsync();

            if (likes.Any())
            {
                var result = likes.Select(x =>
                    new UserDTO
                    {
                        UserName = x.User.UserName                       
                    }).ToList();

                return result;
            }
            return null;
        }
    }
}
