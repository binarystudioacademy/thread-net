﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Extensions;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;


namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        public CommentService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }
        public async Task UpdateComment(UpdateCommentDTO commentDTO, int userId)
        {
            var comment = await _context.Comments.FirstOrDefaultAsync(c => c.Id == commentDTO.Id);
            if (comment == null)
            {
                throw new NotFoundException(nameof(Post), commentDTO.Id);
            }

            if (!comment.IsAuthorComment(userId))
            {
                throw new NoAccessException();
            }

            comment.Body = commentDTO.Body;
            comment.UpdatedAt = DateTime.Now;

            _context.Comments.Update(comment);
            await _context.SaveChangesAsync();
           

        }
       
        public async Task DeleteComment(int commentId, int authorId)
        {
            var comment = await _context.Comments.FirstOrDefaultAsync(c => c.Id == commentId);

            if (comment == null)
            {
                throw new NotFoundException(nameof(Post), commentId);
            }

            if (!comment.IsAuthorComment(authorId))
            {
                throw new NoAccessException();
            }
            
            _context.Comments.Remove(comment);
            await _context.SaveChangesAsync();
        }       
    }
}